# Simple Node.js multi-page app (MPA) rendering strategy sample - With express

MPAs are perhaps the easiest rendering strategy. Each time the user clicks on a link or submits a form, a full-page refresh is performed.

In this branch, Node.js is used with express to load the files and handle responding to requests. Compare this solution to the one without express by switching branches.

## Installation & running instructions

Pull the required packages.

```
npm install
```

Start the server.

```
node index.js
```

Navigate to ```localhost:3000``` with your browser to visit the site.

## Change to branch without express

You can check out how much simpler this code is compared with only using Node.js by changing the branch to ```without-express```.

```
git checkout without-express
```