const express = require('express'); //This simplifies handling of requests and responses

const app = express(); //Create a new express server

app.use(express.static(__dirname)); //Serve static files

//We don't need to specify any routes here as visiting localhost:3000 return index.html by default,
//localhost:3000/about.html looks for a static file names about.html and so on.
//However, if you try to request some other endpoint (i.e., not one matching a static file name), you will get an error.

const server = app.listen(3000, () => {
    console.log(`Server is running on port ${server.address().port}`);
});